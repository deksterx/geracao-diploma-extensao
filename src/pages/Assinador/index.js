import React, { useEffect, useState } from "react";
import Select from "react-select";
import api from "../../services/api";
import * as utils from "../../utils/utils.js";

import Extension from '../../components/Extension'

export default function Assinador() {

  // ESTADOS ONDE AS VARIÁVIES E VALORES DOS INPUTS FICAM ARMAZENADAS
  const [certificadosInstalados, setCertificadosInstalados] = useState("");
  const [certificadoSelecionado, setCertificadoSelecionado] = useState("");
  const [tipoAssinante, setTipoAssinante] = useState("PessoaInstituicao");
  const [tipoDocumento, setTipoDocumento] = useState("XMLPublico")
  const [assinando, setAssinando] = useState(false)

  // VERIFICA SE A EXTENSÃO DA BRY ESTÁ INSTALADA
  const extensaoInstalada = utils.isExtensionInstalled();

  useEffect(() => {
    setTipoAssinante("IESEmissora");
  }, [tipoDocumento])

  // USANDO A EXTENSÃO, VERIFICA OS CERTIFICADOS INSTALADOS E ARMAZENA NO ESTADO DA APLICAÇÃO
  useEffect(() => {
    if (extensaoInstalada) {
      window.BryExtension.listCertificates().then(certificados => {
        certificados.forEach(certificado => {
          certificado.label = certificado.name;
        });
        setCertificadosInstalados(certificados);
      });
    }
  }, [extensaoInstalada]);

  // FUNÇÃO QUE SERÁ EXECUTADA AO ENVIAR O FORMULÁRIO
  function enviarFormulario(event) {
    event.preventDefault();
    setAssinando(true);
    assinar();
  }

  // FUNÇÃO QUE SERÁ CHAMADA PELO FORMULÁRIO PARA ASSINAR O DOCUMENTO
  async function assinar() {

    // CRIA UM FORMDATA QUE SERÁ ENVIADO PARA O BACKEND
    const dados = {
      certificate: certificadoSelecionado.certificateData,
      tipoDocumento,
      tipoAssinante,
    }
    // FAZ A REQUISIÇÃO DE INICIALIZAÇÃO DE ASSINATURA PARA O BACKEND
    try {
      const resposta = await api.post(`/${tipoDocumento}/inicializa`, dados);
      console.log(resposta.data);
    
    // SE A INICIALIZAÇÃO OCORREU DE MANEIRA CORRETA, CHAMA A FUNÇÃO QUE IRÁ CIFRAR OS DADOS USANDO A EXTENSÃO
      assinarExtensao(resposta.data, certificadoSelecionado.certificateData);
    } catch (err) {
      console.log(err.resposta);
      alert(err.resposta);
    }
  }
  async function assinarExtensao(respostaInicializacao, certificado) {

    console.log(respostaInicializacao);

    const inputExtension = {};

    // CONFIGURA O O OBJETO QUE RETORNOU DA INICIALIZAÇÃO DA MANEIRA QUE A EXTENSÃO RECEBE
    inputExtension.formatoDadosEntrada = 'Base64'
    inputExtension.formatoDadosSaida = 'Base64'
    inputExtension.algoritmoHash = 'SHA256'
    inputExtension.assinaturas = [{}];
    inputExtension.assinaturas[0].hashes = respostaInicializacao.signedAttributes[0].content;
    inputExtension.assinaturas[0].nonce = respostaInicializacao.signedAttributes[0].nonce;
    
    inputExtension.assinaturas.forEach(
      assinatura => (assinatura.hashes = [assinatura.hashes])
    );

    console.log(inputExtension);

    // USA A EXTENSÃO PARA CIFRAR OS DADOS
    await window.BryExtension.sign(
      certificadoSelecionado.certId,
      JSON.stringify(inputExtension)
    ).then(async assinatura => {

      console.log("assinatura")
      console.log(assinatura)

      // SE OS DADOS FORAM CIFRADOS, CRIA UM FORMDATA QUE SERÁ ENVIADO PARA A FINALIZAÇÃO
      const dados = {
        initializedDocuments: respostaInicializacao.initializedDocuments[0].content,
        certificate: certificado,
        cifrado: assinatura.assinaturas[0].hashes[0],
        tipoAssinante,
        tipoDocumento,
      }
      // FAZ A REQUISIÇÃO DE FINALIZAÇÃO PARA O BACKEND
      try {
        const respostaRequisicao = await api.post(`/${tipoDocumento}/finaliza`, dados);
        
        setAssinando(false);
        if(respostaRequisicao.data.documentos) {
          window.location.href = respostaRequisicao.data.documentos[0].links[0].href;
        } else {
          // FAZ DOWNLOAD DO ARQUIVO ASSINADO
          alert("Retorno em Base64 (Logado no console)");
          console.log("Retorno: " + respostaRequisicao.data);
        }
      } catch (err) {
        alert(err);
      }
    }).catch( err => {
      console.log(err);
    });
  }

  // HTML DO FRONTEND
  return (
    <>
      {extensaoInstalada ? (
        <form onSubmit={enviarFormulario}>
          <h2>Assinador de Diploma Digital</h2>
          <label htmlFor="certificate">Selecione o certificado que deseja utilizar para assinar</label>
          <Select
            id="certificate"
            options={certificadosInstalados}
            onChange={event => setCertificadoSelecionado(event)}
            value={certificadoSelecionado}
            required
          />

          {certificadoSelecionado ? (
            <React.Fragment>
              <input
                type="text"
                readOnly
                value={certificadoSelecionado.issuer || ""}
              />
              <input
                type="text"
                readOnly
                value={certificadoSelecionado.expirationDate || ""}
              />
              <input
                type="text"
                readOnly
                value={certificadoSelecionado.certificateType || ""}
              />
              <textarea
                type="text"
                rows="5"
                value={certificadoSelecionado.certificateData}
                readOnly
              />
            </React.Fragment>
          ) : (
            ""
          )}
          <label htmlFor="tipoDocumento">Tipo de Documento</label>
          <select
            name="tipoDocumento"
            value={tipoDocumento}
            onChange={event => setTipoDocumento(event.target.value)}
          >
            <option value="XMLPublico">XML Diplomado</option>
            <option value="XMLPrivado">XML Documentação Acadêmica</option>
          </select>

          <label htmlFor="tipoAssinante">Tipo de Assinante</label>
          <select
            name="tipoAssinante"
            value={tipoAssinante}
            onChange={event => setTipoAssinante(event.target.value)}
          >
                <option value="IESEmissora">IES Emissora</option>
                <option value="IESRegistradora">IES Registradora</option>
            {tipoDocumento === "XMLPublico" ? (
              <>
                <option value="IESRepresentantes">IES Representantes</option>
                <option value="PessoasFisicas">Pessoas Físicas</option>
                ) 
                </> ) : ""}
          </select>

          <button className="btn" type="submit">
            Assinar
          </button>

          <label>
              {assinando ? "Realizando a assinatura do documento..." : ""}
          </label>
        </form>

      // SE A EXTENSÃO NÃO ESTIVAR INSTALADA, RENDERIZA O COMPONENTE DE MANUAL DE INSTALAÇÃO
      ) : (<Extension/>)}
    </>
  );
}
